#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>


#define l 288
#define e 4


struct Ausw
{
	float degree[4];
	time_t timestamp;
	char realTime[64];
};

///////////////////
///One-dimension///
///////////////////

void waitFor(unsigned int secs);

void genNumber(Ausw area[], int n);

float maxNumber(float temp[], int n);

float minNumber(float temp[], int n);

float averageTemp(float temp[], int n);

int inRange(float temp[], float min, float max, int n);

void changeToHms(int time[], int timeMinutes);
FILE* fp;

///////////////////
//Multi-dimension//
///////////////////


void maxNumberDim(Ausw area[], float max[], int n);

void minNumberDim(Ausw area[], float min[], int n);

void averageTempDim(Ausw area[], float average[], int n);

void averageTempDimCom(Ausw area[], int n);

void standardDeviation(Ausw area[], int  n, float stdAbweichung[], float average[]);

void changeTimeValue(Ausw area[], int n);

void output(Ausw area[], float max[], float min[], float stdAbwe[], float average[]);

void output_save(Ausw area[], float max[], float min[], float stdAbwe[], float average[]);


int main()
{

	////////////////
	//Declare Area//
	////////////////

	FILE *fp;
	fopen_s(&fp, "Temp_Data.txt", "w");
	
	//Struct
	Ausw Speicher[l];

	//Array
	float temp[l][e];
	//array hours=[0];minutes=[1]
	int time[2];
	int timest[l];
	//values
	float min[3], max[3], average[3];
	float stdAbwe[3];

	////////////////
	//Program Area//
	////////////////
	genNumber(Speicher, l);
	maxNumberDim(Speicher, max, l);
	minNumberDim(Speicher, min, l);
	averageTempDim(Speicher, average, l);
	averageTempDimCom(Speicher, l);
	standardDeviation(Speicher, l, stdAbwe, average);
	changeTimeValue(Speicher, l);

	output(Speicher, max, min, stdAbwe, average);
	output_save(Speicher, max, min, stdAbwe, average);
	fclose(fp);
}


///////////////////
///One-dimension///
///////////////////

void waitFor(unsigned int secs) {
		unsigned int retTime = time(0) + secs;   // Get finishing time.
		while (time(0) < retTime);               // Loop until it arrives.
	}

void genNumber(Ausw area[], int n)
	{
		srand(time(NULL));
		//fill the array with random numbers
		for (int j = 0; j < n; j++)
		{
			for (int i = 0; i < 3; i++)
			{
				//create random number between 200-285
				float zufallszahl = ((rand() % 86 + 200));
				//divide by 10 to get 20.0-28.5
				float temperatur = zufallszahl / 10;

				area[j].degree[i] = temperatur;

				waitFor(0.5);
				time_t now;
				area[j].timestamp = time(&now);

			}
		}
	}

float maxNumber(float temp[], int n)
	{
		//declare max as first space from the array
		float max = temp[0];
		//test the max number /if there is abigger exchange with it
		for (int i = 1; i < n; i++)
			if (temp[i] > max)
				max = temp[i];
		return max;
	}

float minNumber(float temp[], int n)
	{
		//declare min as first space from the array
		float min = temp[0];
		//test the min number /if there is a smaller exchange with it
		for (int i = 1; i < n; i++)
			if (temp[i] < min)
				min = temp[i];
		return min;
	}

float averageTemp(float temp[], int n) {
		float durschnitt = 0;
		//add all temperatures
		for (int i = 0; i < n; i++) {

			durschnitt = durschnitt + temp[i];
		}
		//divide by the total number
		durschnitt = durschnitt / n;
		return (durschnitt);
	}

int inRange(float temp[], float min, float max, int n)
	{
		float timeF = 0;
		int timeI;
		//check at which time the temp is in the range
		for (int i = 0; i < n; i++)
		{
			if ((temp[i] < max) && (temp[i] > min))
				timeF++;
		}
		//multiply the times by 5, as the measurement is made at these distances
		timeF = timeF * 5;
		return timeI = timeF;
	}

void changeToHms(int time[], int timeMinutes)
	{
		//modulo of the minutes by 60 to get the minutes
		time[1] = timeMinutes % 60;
		//subtrect the minutes to get full hours and the divide by 60 to get the hours
		time[0] = (timeMinutes - time[1]) / 60;
	}
	

///////////////////
//Multi-dimension//
///////////////////


void maxNumberDim(Ausw area[], float max[], int n)
{
	for (int j = 0; j < 3; j++)
	{
		float maxi = area[0].degree[j];

		//test the max number /if there is abigger exchange with it
		for (int i = 1; i < n; i++)
			if (area[i].degree[j] > maxi)
				maxi = area[i].degree[j];

		max[j] = maxi;
	}
}

void minNumberDim(Ausw area[], float min[], int n)
{
	for (int j = 0; j < 3; j++)
	{
		float mini = area[0].degree[j];

		//test the min number /if there is abigger exchange with it
		for (int i = 1; i < n; i++)
			if (area[i].degree[j] < mini)
				mini = area[i].degree[j];

		min[j] = mini;
	}
}

void averageTempDim(Ausw area[], float average[], int n)
{
	for (int j = 0; j < 3; j++)
	{
		float durschnitt = 0;
		//add all temperatures
		for (int i = 0; i < n; i++) {

			durschnitt = durschnitt + area[i].degree[j];
		}
		//divide by the total number
		average[j] = durschnitt / n;
	}
}

void averageTempDimCom(Ausw area[], int n)
{

	//add all 3 temperatures
	for (int i = 0; i < n; i++)
	{
		float durschnitt = 0;
		for (int j = 0; j < 3; j++)
		{
			durschnitt = durschnitt + area[i].degree[j];
			//divide by the total number
		}
		area[i].degree[3] = durschnitt / 3;
	}
}

void standardDeviation(Ausw area[], int  n, float stdAbweichung[], float average[])
{
	for (int i = 0; i < e - 1; i++)
	{
		float varianz = 0;
		for (int j = 0; j < n; j++)
		{
			varianz = varianz + (area[j].degree[i] - average[i]) * (area[j].degree[i] - average[i]);
		}
		varianz = varianz / (n - 1);
		stdAbweichung[i] = sqrt(varianz);
	}
}

void changeTimeValue(Ausw area[], int n)
{
	char localtime[64];
	tm localtimetm;

	for (int i = 0; i < n; i++)
	{
		localtime_s(&localtimetm, &area[i].timestamp);
		asctime_s(localtime, 64, &localtimetm);
		for (int j = 0; j < 64; j++)
		{
			area[i].realTime[j] = localtime[j];
		}
	}
}

void output(Ausw area[], float max[], float min[], float stdAbwe[], float average[])
{
	//Values
	for (int i = 0; i < l; i++)
	{
		printf("\n%i. Wert:", i + 1);
		for (int j = 0; j < 4; j++)
		{
			printf(" %.2f C   ", area[i].degree[j]);
			if (j == 3)   //time
			{
				printf("     Zeit: %s \n", area[i].realTime);
			}
		}
	}

	//Max Values
	printf("\n Max Wert:");
	for (int j = 0; j < 3; j++)
		printf(" %.2f C   ", max[j]);
	// min Values
	printf("\n Min Wert:");
	for (int j = 0; j < 3; j++)
		printf(" %.2f C   ", min[j]);
	//average Values
	printf("\n Durschnitts Wert:");
	for (int j = 0; j < 3; j++)
		printf(" %.2f C   ", average[j]);
	//average Values
	printf("\n Standardabweichung Wert:");
	for (int j = 0; j < 3; j++)
		printf(" %.2f C   ", stdAbwe[j]);
}

void output_save(Ausw area[], float max[], float min[], float stdAbwe[], float average[])
{
	//Values
	for (int i = 0; i < l; i++)
	{
		fprintf(fp, "\n%i. Wert:", i + 1);
		for (int j = 0; j < 4; j++)
		{
			fprintf(fp, " %.2f C   ", area[i].degree[j]);
			if (j == 3)   //time
			{
				fprintf(fp, "     Zeit: %s \n", area[i].realTime);
			}
		}
	}

	//Max Values
	fprintf(fp, "\n Max Wert:");
	for (int j = 0; j < 3; j++)
		fprintf(fp, " %.2f C   ", max[j]);
	// min Values
	fprintf(fp, "\n Min Wert:");
	for (int j = 0; j < 3; j++)
		fprintf(fp, " %.2f C   ", min[j]);
	//average Values
	fprintf(fp, "\n Durschnitts Wert:");
	for (int j = 0; j < 3; j++)
		fprintf(fp, " %.2f C   ", average[j]);
	//average Values
	fprintf(fp, "\n Standardabweichung Wert:");
	for (int j = 0; j < 3; j++)
		fprintf(fp, " %.2f C   ", stdAbwe[j]);
}

